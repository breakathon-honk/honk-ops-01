# Honk Ops 01
## Install flux

export GITLAB_TOKEN=<redacted>

flux bootstrap gitlab \
    --ssh-hostname=gitlab.com \
    --owner=breakathon-honk \
    --repository=honk-fun-02 \
    --branch=main \
    --path=clusters/kbreak-fun-honk


## Honk Service URL

* https://ingress-svc.honk-ops.k8slab.org
* http://lb-svc.honk-ops.k8slab.org
* http://nodeport-svc.honk-ops.k8slab.org:32391
